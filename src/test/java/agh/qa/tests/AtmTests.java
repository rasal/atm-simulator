package agh.qa.tests;

import agh.qa.Account;
import agh.qa.Atm;
import agh.qa.DebitCard;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

public class AtmTests {
  private Account _testAccount;
  private DebitCard _testCard;
  private Atm _testAtm;

  @BeforeMethod
  public void SetUp(){
    // preconditions
    _testAccount = new Account(500);
    _testCard = new DebitCard(_testAccount,1234);
    _testAtm = new Atm();
  }

  @Test
  public void TestWithdrawingCorrectAmountCorrectPin(){
    _testAtm.insertCard(_testCard);
    _testAtm.enterPin(1234);
    _testAtm.withdraw(100);

    Assert.assertEquals(_testAccount.getBalance(), 400.0, "Verify current account balance.");
    Assert.assertEquals(_testCard.isBlocked(), false, "Verify card is blocked.");
  }

  @Test
  public void TestWithdrawingCorrectAmountIncorrectPin(){
    _testAtm.insertCard(_testCard);
    _testAtm.enterPin(12345);
    _testAtm.withdraw(100);

    Assert.assertEquals(_testAccount.getBalance(), 500.0, "Verify current account balance.");
    Assert.assertEquals(_testCard.isBlocked(), false, "Verify card is blocked.");
  }

  @Test
  public void TestEnteringIncorrectPinThreeTimes(){
    _testAtm.insertCard(_testCard);
    _testAtm.enterPin(12345);
    _testAtm.enterPin(12346);
    _testAtm.enterPin(12347);
    _testAtm.withdraw(100);

    Assert.assertEquals(_testAccount.getBalance(), 500.0, "Verify current account balance.");
    Assert.assertEquals(_testCard.isBlocked(), true, "Verify card is blocked.");
  }

  @Test
  public void TestWithdrawing50AmountCorrectPin(){
    _testAtm.insertCard(_testCard);
    _testAtm.enterPin(1234);
    _testAtm.withdraw(50);

    Assert.assertEquals(_testAccount.getBalance(), 450.0, "Verify current account balance.");
    Assert.assertEquals(_testCard.isBlocked(), false, "Verify card is blocked.");
  }

  @Test
  public void TestWithdrawing200AmountCorrectPin(){
    _testAtm.insertCard(_testCard);
    _testAtm.enterPin(1234);
    _testAtm.withdraw(200);

    Assert.assertEquals(_testAccount.getBalance(), 300.0, "Verify current account balance.");
    Assert.assertEquals(_testCard.isBlocked(), false, "Verify card is blocked.");
  }

  @Test
  public void TestWithdrawing350AmountCorrectPin(){
    _testAtm.insertCard(_testCard);
    _testAtm.enterPin(1234);
    _testAtm.withdraw(350);

    Assert.assertEquals(_testAccount.getBalance(), 150.0, "Verify current account balance.");
    Assert.assertEquals(_testCard.isBlocked(), false, "Verify card is blocked.");
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void TestWithdrawingIncorrectAmountCorrectPin(){
    _testAtm.insertCard(_testCard);
    _testAtm.enterPin(1234);
    _testAtm.withdraw(40);

    Assert.assertEquals(_testAccount.getBalance(), 500.0, "Verify current account balance.");
    Assert.assertEquals(_testCard.isBlocked(), false, "Verify card is blocked.");
  }

  @Test(expectedExceptions = IllegalArgumentException.class)
  public void TestWithdrawingTooMuchAmountCorrectPin(){
    _testAtm.insertCard(_testCard);
    _testAtm.enterPin(1234);
    _testAtm.withdraw(1000);

    Assert.assertEquals(_testAccount.getBalance(), 500.0, "Verify current account balance.");
    Assert.assertEquals(_testCard.isBlocked(), false, "Verify card is blocked.");
  }

  @Test
  public void TestWithdrawingAllAmountCorrectPin(){
    _testAtm.insertCard(_testCard);
    _testAtm.enterPin(1234);
    _testAtm.withdraw(500);

    Assert.assertEquals(_testAccount.getBalance(), 0.0, "Verify current account balance.");
    Assert.assertEquals(_testCard.isBlocked(), false, "Verify card is blocked.");
  }
}
