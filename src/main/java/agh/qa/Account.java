package agh.qa;

public class Account {
  private double _balance;

  public Account (double balance){
    _balance=balance;
  }

  public double getBalance(){
    return _balance;
  }

  public void withdraw(int money) {
    _balance -= money;
  }
}
