package agh.qa;

public class DebitCard {
  private Account _connectedAccount;
  private int _pin;
  private boolean _isBlocked = false;

  public DebitCard(Account account,int pin){
    _connectedAccount=account;
    _pin=pin;
  }

  public boolean isPinCorrect(int pin){
    return pin == _pin;
  }

  public void block(){
    _isBlocked = true;
    System.out.println("Your card has been blocked.");
  }

  public boolean isBlocked(){
    return _isBlocked;
  }

  void withdraw(int money) {
    if(_connectedAccount.getBalance() < money) {
      throw new IllegalArgumentException("Withdraw amount too big");
    }
    _connectedAccount.withdraw(money);
  }
}
