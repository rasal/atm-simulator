package agh.qa;

public class Atm {
  private static final int ATTEMPTS_LIMIT = 3;
  private DebitCard _card;
  private int _pin;
  private int _attempts = 0;

  public void withdraw(int money){
    verifyAllowedBanknotes(money);

    if (_card.isPinCorrect(_pin)) {
      _card.withdraw(money);
    }
  }

  private void verifyAllowedBanknotes(int money) {
    if (money % 50 != 0) {
      throw new IllegalArgumentException("Incorrect withdraw amount");
    }
  }

  public void insertCard(DebitCard card){
    this._card = card;
  }

  public void enterPin(int pin) {
    this._pin = pin;
    if(!_card.isPinCorrect(pin)) {
      _attempts++;
    }
    if(_attempts >= ATTEMPTS_LIMIT) {
      _card.block();
    }
  }
}
