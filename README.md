# WBTProjectTemplate

Project Template for White Box Techniques course.

### Features

* main ATM framework prepared containing required classes
* one failing TestNg test as an example

### What to do?

* Fork this project as your private project
* Implement missing functionalities for ATM to meet specified requirements (below)
* Write unit tests to achieve 100% line coverage
* Commit/push to your project
* Send me an email with link to your repository

### Requirements for ATM
* Correct PIN is required to withdraw money
* When incorrect PIN is entered 3 times, card is blocked
* Available banknotes: 50, 100, 200
* Banknotes count in ATM is infinite
* Amount withdrawn has to be less or equal to account balance
